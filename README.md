Install requirements:
```
pip install -r requirements.txt
```

To run:
```
python main.py
```

run test:
```
./test.sh
```

profile:
```
./profile.sh
```
outputs profile.txt
