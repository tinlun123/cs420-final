from enum import Enum
import numpy as np
import random

class CrossoverType(Enum):
  ONE_POINT = 1
  TWO_POINT = 2



class Weights:
  def __init__(self, numberOfWeights, weightRange):
    self.weights = np.random.rand(numberOfWeights)*weightRange - 0.5*weightRange
    
    
  def tolist(self):
    return self.weights.flatten().tolist()


  def crossoverAllWith(self, other, crossoverProbability=0.9, crossoverType=CrossoverType.ONE_POINT):
    assert(self.weights.shape == other.weights.shape)
    
    if crossoverType == CrossoverType.ONE_POINT:
      left, right = self._crossover1pt(self.weights, other.weights, crossoverProbability)
    elif crossoverType == CrossoverType.TWO_POINT:
      left, right = self._crossover2pt(self.weights, other.weights, crossoverProbability)
    else:
      raise ValueError("Crossover type unknown")
    self.weights = left
    other.weights = right
        
      
  def mutate(self, mutationProbability=0.05):
    for i in range(len(self.weights)):
      self.weights[i] = self._mutate(self.weights[i], mutationProbability)
      

  def _crossover1pt(self, left, right, crossoverProbability):
    if crossoverProbability < random.random():
      return left, right

    assert(len(left) == len(right))
    
    crossoverPoint = random.randint(2, len(left)-1)
    newLeft = np.concatenate((left[:crossoverPoint], right[crossoverPoint:]))
    newRight = np.concatenate((right[:crossoverPoint], left[crossoverPoint:]))
    
    return newLeft, newRight
  
  
  def _crossover2pt(self, left, right, crossoverProbability):
    if crossoverProbability < random.random():
      return left, right

    assert(len(left) == len(right))
    
    crossoverPoint1 = random.randint(2, len(left)-2)
    crossoverPoint2 = random.randint(crossoverPoint1, len(left)-1)
    print("corssover 2pt")
    print(crossoverPoint1,crossoverPoint2)
    newLeft = np.concatenate((left[:crossoverPoint1],
                              right[crossoverPoint1:crossoverPoint2],
                              left[crossoverPoint2:]))
    newRight = np.concatenate((right[:crossoverPoint1],
                               left[crossoverPoint1:crossoverPoint2],
                               right[crossoverPoint2:]))
    
    return newLeft, newRight

  
  def _mutate(self, weight, mutationProbability, mutationScale=.2):
    # mutationScale: can mutate up to mutationScale*100% of weight.

    if mutationProbability < random.random() :
      return weight

    mutation = (random.random() * weight*mutationScale*2) - (weight*mutationScale)
    return weight + mutation
