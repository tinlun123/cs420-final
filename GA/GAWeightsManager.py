from GA.Weights import Weights
from enum import Enum
import random
import numpy as np

class SelectionType(Enum):
  TOURNAMENT = 1
  BINARY_TOURNAMENT = 2
  ROULETTE = 3
  
  
  
class GAWeightsManager:
  def __init__(self, population, numberOfWeights, weightRange):
    self.weights = []
    self._newWeights = []
    for i in range(population):
      self.weights.append(Weights(numberOfWeights, weightRange))
    self._newWeights = [None]*population
  
  
  def tolist(self):
    ls = []
    for weight in self.weights:
      ls += weight.tolist()
    return ls


  def getWeightsOfInd(self, ind):
    return self.weights[ind].tolist()
    
  

  def saveBestGeneration(self, allFitness, weights):
    leastFit = np.argwhere(allFitness == np.min(allFitness))[0][0]
    self.weights[leastFit] = weights

    
  def getBestGeneration(self, allFitness):
    mostFit = np.argwhere(allFitness == np.max(allFitness))[0][0]
    return self.weights[mostFit]

  def fitness(self, value):
    # Currently returns input value.
    # In the future the fitness function can be specified by user.
    return value

  
  def convertInputToFitness(self, fitnessInputs):
    return np.asarray([self.fitness(value) for value in fitnessInputs])

      
  def nextGeneration(self, fitnessInputs, selectionType=SelectionType.TOURNAMENT, withElitism=True):
    if withElitism:
      best_weights = self.getBestGeneration(fitnessInputs)
      
    if selectionType == SelectionType.TOURNAMENT:
      self._selectionTournament(fitnessInputs)
    elif selectionType == SelectionType.BINARY_TOURNAMENT:
      self._selectionBinaryTournament(fitnessInputs)
    elif selectionType == SelectionType.ROULETTE:
      self._selectionRoulette(fitnessInputs)
    else:
      raise ValueError("Tournament type unknown")
    self._generateNewGeneration()
    
    if withElitism:
      self.saveBestGeneration(fitnessInputs, best_weights)
    
   
  def _selectionBinaryTournament(self, fitnessInputs):
    self._selectionTournament(fitnessInputs, poolSample=2)


  def _selectionTournament(self, fitnessInputs, poolSample=10):
    allFitness = self.convertInputToFitness(fitnessInputs)
    for i in range(len(self._newWeights)):
      tournamentPoolIndex = np.random.choice(len(fitnessInputs), poolSample)
      mostFit = -np.inf
      mostFitWeight = None
      for index in tournamentPoolIndex:
        if allFitness[index] > mostFit:
          mostFit = allFitness[index]
          mostFitWeight = self.weights[index]
      self._newWeights[i] = mostFitWeight
    

  def _selectionRoulette(self, fitnessInputs):
    allFitness = self.convertInputToFitness(fitnessInputs)
    fitnessSum = np.cumsum(allFitness)
    for i in range(len(self.weights)):
      fitnessValue = random.random() * fitnessSum[-1]
      fitIndex = np.argwhere(fitnessSum > fitnessValue)[0][0]
      self._newWeights[i] = self.weights[fitIndex]
    

  def _generateNewGeneration(self):
    for i in range(0, len(self._newWeights)-1, 2):
      self._newWeights[i].crossoverAllWith(self._newWeights[i+1])
      self._newWeights[i].mutate()
      self._newWeights[i+1].mutate()
      self.weights[i].weights = self._newWeights[i].weights.copy()
      self.weights[i+1].weights = self._newWeights[i+1].weights.copy()
