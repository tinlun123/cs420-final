import pytest
from GA.GAWeightsManager import *
import numpy as np

smallNNsSize = 2
smallNumberOfOutputNodes = 2
smallNumberOfHiddenNodes = 3
smallNumberOfInputNodes = 4
weightRange = 0.1


@pytest.fixture
def small_factory_hidden():
  return GAWeightsManager(smallNNsSize, smallNumberOfHiddenNodes*smallNumberOfInputNodes, weightRange)


@pytest.fixture
def small_factory_output():
  return GAWeightsManager(smallNNsSize, smallNumberOfOutputNodes*(smallNumberOfHiddenNodes+1), weightRange)


@pytest.fixture
def large_factory_hidden():
  return GAWeightsManager(60, 40*100, weightRange)


@pytest.fixture
def large_factory_output():
  return GAWeightsManager(60, 20*(40+1), weightRange)


def test_factory_lengths(small_factory_hidden, small_factory_output):
  assert len(small_factory_hidden.weights) == smallNNsSize
  assert len(small_factory_output.weights) == smallNNsSize
  

def test_generation_saved(small_factory_output):
  bestWeights = small_factory_output.weights[1].weights.copy()
  fitness = np.array([0.2, 0.1])
  small_factory_output.nextGeneration(fitness)
  assert not np.array_equal(small_factory_output.weights[0].weights, bestWeights)
  small_factory_output.saveBestGeneration([0.1, 0.9], bestWeights)
  assert np.array_equal(small_factory_output.weights[0], bestWeights)

  
def test_best_generation_returned(small_factory_output):
  bestWeights = small_factory_output.weights[1].weights.copy()
  assert np.array_equal(small_factory_output.getBestGeneration([0.1, 0.9]).weights, bestWeights)

 
def perform_new_generation(large_factory_hidden, tournamentType):
  nnSize = len(large_factory_hidden.weights)
  prevWeights = large_factory_hidden.weights[59].weights.copy()
  large_factory_hidden.nextGeneration(np.random.rand(nnSize, 1), selectionType=tournamentType)
  large_factory_hidden.nextGeneration(np.random.rand(nnSize, 1), selectionType=tournamentType)
  assert not np.array_equal(large_factory_hidden.weights[59].weights, prevWeights)
 
 
def test_new_generation_is_performed_with_tournament(large_factory_hidden):
  perform_new_generation(large_factory_hidden, SelectionType.TOURNAMENT)


def test_new_generation_is_performed_with_binary_tournament(large_factory_hidden):
  perform_new_generation(large_factory_hidden, SelectionType.BINARY_TOURNAMENT)
  
  
def test_new_generation_is_performed_with_roulette(large_factory_hidden):
  perform_new_generation(large_factory_hidden, SelectionType.ROULETTE)


def test_to_list_size(small_factory_hidden):
  assert len(small_factory_hidden.tolist()) ==  smallNNsSize * \
                                                smallNumberOfHiddenNodes * \
                                                smallNumberOfInputNodes