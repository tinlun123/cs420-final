from neuralnetwork import Network
from old_implementation import *
import numpy as np
import pytest

input_units = 784
hidden_units = 20
output_units = 10
network = Network(input_units, hidden_units, output_units)

weights = np.genfromtxt('tests/weights.csv', delimiter=',')
test_data = np.genfromtxt('tests/mnist_test.csv', delimiter=',')
test_data = np.true_divide(test_data, 255)
test_data = test_data[:, 1:]

def test_weights_required():
  assert network.weights_required() == (input_units+1)*hidden_units + \
                                       (hidden_units+1)*output_units


def test_layers():
  assert len(network.layers) == 3
  network.assign_weights(weights)
  assert network.layers[0].next_layer is network.layers[1]
  assert network.layers[1].next_layer is network.layers[2]
  assert network.layers[2].next_layer is None
  assert network.layers[0].weights.size == (input_units+1)*hidden_units
  assert network.layers[1].weights.size == (hidden_units+1)*output_units
  assert len(network.layers[2].weights) == 0


def test_small_network():
  network = Network(1, 1, 1)
  assert network.weights_required() == 4
  network.assign_weights([0]*4)
  output = network.propagate([1])
  assert output == [0.5]


def test_output():
  # Tests whether the output of the network matches another working 
  # implementation.
  hidden_weights = np.reshape(weights[:(input_units+1)*hidden_units],
                              (hidden_units, input_units+1))
  output_weights = np.reshape(weights[(input_units+1)*hidden_units:],
                              (output_units, hidden_units+1))
  expected_predictions = []
  network_predictions = []
  for i in range(0, len(test_data)):
    hidden_output = prop_forward(test_data[i], hidden_weights, hidden_units)
    activation_values = prop_forward(hidden_output, output_weights,
                                     output_units)
    p = activation_values.index(max(activation_values))
    expected_predictions.append(p)
    
    activation_values = list(network.propagate(test_data[i]))
    p = activation_values.index(max(activation_values))
    network_predictions.append(p)

  assert network_predictions == expected_predictions
