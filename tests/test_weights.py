from GA.Weights import *
import pytest

smallNumberOfOutputNodes = 2
smallNumberOfInputNodes = 5
weightRange = 0.1


@pytest.fixture
def small_weight_one():
  return Weights(4, weightRange)


@pytest.fixture
def small_weight_two():
  return Weights(4, weightRange)


@pytest.fixture
def large_weight_one():
  return Weights(200*200, weightRange)


@pytest.fixture
def large_weight_two():
  return Weights(200*200, weightRange)


def test_weights_length():
  weights = Weights(smallNumberOfOutputNodes*smallNumberOfInputNodes, weightRange)
  assert len(weights.weights) == smallNumberOfOutputNodes*smallNumberOfInputNodes

  
def test_weights_range():
  for k in range(10):
    weights = Weights(smallNumberOfOutputNodes*smallNumberOfInputNodes, weightRange)
    for i in range(len(weights.weights)):
      assert weights.weights[i] >= -weightRange
      assert weights.weights[i] <= weightRange

def perform_crossover(large_weight_one, large_weight_two,crossoverProbability,crossoverType): 
  previousWeightsOne = large_weight_one.weights.copy()
  previousWeightsTwo = large_weight_two.weights.copy()
  large_weight_one.crossoverAllWith(large_weight_two, crossoverProbability,crossoverType)
  for i in range(len(large_weight_one.weights)):
      assert not np.array_equal(large_weight_one.weights, previousWeightsOne)
      
  for i in range(len(large_weight_two.weights)):
      assert not np.array_equal(large_weight_two.weights, previousWeightsTwo)
      
  
def test_crossover_1pt_changes(large_weight_one, large_weight_two):
  perform_crossover(large_weight_one, large_weight_two,1,CrossoverType.ONE_POINT)
     
        
def test_crossover_2pt_changes(large_weight_one, large_weight_two):
  perform_crossover(large_weight_one, large_weight_two,1,CrossoverType.TWO_POINT)


def test_crossover_not_changes(large_weight_one, large_weight_two):
  previousWeightsOne = large_weight_one.weights.copy()
  previousWeightsTwo = large_weight_two.weights.copy()
  large_weight_one.crossoverAllWith(large_weight_two, crossoverProbability=0)
  for i in range(len(large_weight_one.weights)):
      assert np.array_equal(large_weight_one.weights[i], previousWeightsOne[i])
      
  for i in range(len(large_weight_two.weights)):
      assert np.array_equal(large_weight_two.weights[i], previousWeightsTwo[i])


def test_mutate_changes(large_weight_one):
  previousWeightsOne = large_weight_one.weights.copy()
  large_weight_one.mutate(mutationProbability=1)
  for i in range(len(large_weight_one.weights)):
      assert large_weight_one.weights[i] != previousWeightsOne[i]


def test_mutate_not_changes(large_weight_one):
  previousWeightsOne = large_weight_one.weights.copy()
  large_weight_one.mutate(mutationProbability=0)
  for i in range(len(large_weight_one.weights)):
      assert large_weight_one.weights[i] == previousWeightsOne[i]


def test_to_list_size(small_weight_one):
  assert len(small_weight_one.tolist()) == 4