import numpy as np
from scipy.special import expit
import math
from datetime import datetime
#from sklearn.model_selection import train_test_split
from GA.Weights import Weights
from GA.GAWeightsManager import GAWeightsManager
np.set_printoptions(suppress=True)
learning_rate = 0.2
max_epoch = 30
output_amount = 2
hidden_units = 3
weight_range = 0.15



def get_accuracy(ls1, ls2):
  # Returns percent match between ls1 and ls2.
  assert(len(ls1) == len(ls2))
  correct = 0
  for a, b in zip(ls1, ls2):
    if a == b:
      correct += 1
  return correct / len(ls1)


def get_confusion_matrix(target, prediction):
  # Returns the matrix of actual target class to prediction.
  # matrix[target][prediction]

  assert(len(target) == len(prediction))
  matrix = np.zeros((10, 10))
  for a, b in zip(target, prediction):
    matrix[int(a)][int(b)] += 1
  return matrix     


def sigmoid(z):
  # Unused because I'm using scipy's expit which is faster.
  return 1/(1 + math.exp(-z))

assert(sigmoid(0) == 0.5)


#def prop_forward(inputs, weights, output_amount):
#  # Call multiple times for multiple layers.
#  assert(len(weights[0]) == len(inputs))  # Extra weight for bias.
#  activation_values = [np.dot(weights[i], inputs) for i in range(output_amount)]
#  return expit(activation_values)

def prop_forward(inputs, weights, output_amount):
  # Forward propagation to the next layer.
  # Call multiple times for multiple layers.
  assert(len(weights[0]) == len(inputs) + 1)  # Extra weight for bias.
  inputs = np.insert(inputs, 0, 1)  # Extra input for bias.
  activation_values = []
  for i in range(0, output_amount):
    # Calculating activation value.
    av = sigmoid(np.dot(weights[i], inputs))
    activation_values.append(av)

  return activation_values


# calculate_errors(hidden_output, activation_values, outputWeightsGA.weights[i].weights, targets[n])
def calculate_errors(hidden_values, output_values, output_weights, target):
  hidden_values = hidden_values[1:]  # Remove bias unit.
  assert(len(hidden_values) == hidden_units)
  assert(len(output_values) == output_amount)
  
  output_error = []
  hidden_error = []

  for values, i in zip(output_values, range(len(output_values))):
    
    t = 0.9 if target == i else 0.1
    e = ((t-values)**2)/2#values * (1 - values) * (t - values)
    output_error.append(e)

  for i in range(len(hidden_values)):
    hv = hidden_values[i]
    e = (np.dot(output_weights[:, i+1], output_error)**2)/2#hv * (1 - hv) * np.dot(output_weights[:, i+1], output_error)
    hidden_error.append(e)

  return (hidden_error, output_error)


def change_weights(inputs, output_weights, output_errors):
  assert(output_weights.size == ((len(inputs)) * len(output_errors)))
  weight_change = np.outer(output_errors, inputs) * learning_rate
  return output_weights + weight_change
