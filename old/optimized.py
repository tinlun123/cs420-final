import numpy as np
from scipy.special import expit
import math

np.set_printoptions(suppress=True)
learning_rate = 0.1
max_epoch = 10
output_amount = 10
hidden_units = 10
weight_range = 0.1


def get_accuracy(ls1, ls2):
  # Returns percent match between ls1 and ls2.
  assert(len(ls1) == len(ls2))
  correct = 0
  for a, b in zip(ls1, ls2):
    if a == b:
      correct += 1
  return correct / len(ls1)


def get_confusion_matrix(target, prediction, max_class):
  # Returns the matrix of actual target class to prediction.
  # matrix[target][prediction]

  assert(len(target) == len(prediction))
  matrix = np.zeros((max_class, max_class))
  for a, b in zip(target, prediction):
    matrix[int(a)][int(b)] += 1
  return matrix     


def sigmoid(z):
  # Unused because I'm using scipy's expit which is faster.
  return 1/(1 + math.exp(-z))

assert(sigmoid(0) == 0.5)


def prop_forward(inputs, weights, output_amount):
  # Call multiple times for multiple layers.
  assert(len(weights[0]) == len(inputs))  # Extra weight for bias.
  activation_values = [np.dot(weights[i], inputs) for i in range(output_amount)]
  return expit(activation_values)


def calculate_errors(hidden_values, output_values, output_weights, target):
  hidden_values = hidden_values[1:]  # Remove bias unit.
  assert(len(hidden_values) == hidden_units)
  assert(len(output_values) == output_amount)
  
  output_error = []
  hidden_error = []

  for values, i in zip(output_values, range(len(output_values))):
    t = 0.9 if target == i else 0.1
    e = values * (1 - values) * (t - values)
    output_error.append(e)

  for i in range(len(hidden_values)):
    hv = hidden_values[i]
    e = hv * (1 - hv) * np.dot(output_weights[:, i+1], output_error)
    hidden_error.append(e)

  return (hidden_error, output_error)


def change_weights(inputs, output_weights, output_errors):
  assert(output_weights.size == ((len(inputs)) * len(output_errors)))
  weight_change = np.outer(output_errors, inputs) * learning_rate
  return output_weights + weight_change
  

if __name__ == '__main__':

  print("Reading files...")
  data = np.genfromtxt('mini_train.csv', delimiter=',')
  test_data = np.genfromtxt('mnist_test.csv', delimiter=',')
  print("Reading done!")
  

  print("Preprocessing data...")
  # Shuffle input order.
  np.random.shuffle(data)

  # Extract the target column.
  targets = data[:, 0]
  data = np.true_divide(data, 255)
  test_targets = test_data[:, 0]
  test_data = np.true_divide(test_data, 255)

  # Replace the first column to be the bias column.
  data[:, 0] = 1
  test_data[:, 0] = 1

  sample_amount = len(data)
  test_sample_amount = len(test_data)
  print("Training set: " + str(sample_amount))
  print("Test set: " + str(test_sample_amount))
  assert(len(data[0] == 785))  # Input size includes bias.
  assert(len(test_data[0] == 785))
  input_size = len(data[0])

  hidden_weights = np.random.rand(hidden_units, input_size)*weight_range - 0.5*weight_range
  output_weights = np.random.rand(output_amount, hidden_units+1)*weight_range - 0.5*weight_range
  print("Preprocessing done!")


  accuracy = []

  # Main epoch loop.
  for epoch in range(0, max_epoch+1):
    print("Epoch " + str(epoch) + "...")
    # Training loop.
    predictions = []
    for n in range(0, sample_amount):
      hidden_output = prop_forward(data[n], hidden_weights, hidden_units)
      hidden_output = np.insert(hidden_output, 0, 1)  # Extra input for bias.
      activation_values = prop_forward(hidden_output, output_weights, 
                       output_amount)

      # 0th epoch does not learn.
      if epoch != 0: 
        hidden_errors, output_errors = calculate_errors(hidden_output, 
          activation_values, output_weights, targets[n])

        output_weights = change_weights(hidden_output, output_weights, output_errors)
        hidden_weights = change_weights(data[n], hidden_weights, hidden_errors)


      # Finding prediction.
      activation_values = list(activation_values)
      p = activation_values.index(max(activation_values))
      predictions.append(p)
    assert(len(predictions) == len(targets))
    print("Training done!")

    # Test loop.
    test_predictions = []
    for n in range(0, test_sample_amount):
      hidden_output = prop_forward(test_data[n], hidden_weights, hidden_units)
      hidden_output = np.insert(hidden_output, 0, 1)  # Extra input for bias.
      activation_values = prop_forward(hidden_output, output_weights, 
                       output_amount)

      # Finding prediction.
      activation_values = list(activation_values)
      p = activation_values.index(max(activation_values))
      test_predictions.append(p)
    assert(len(test_predictions) == len(test_targets))
    print("Test set done!")

    accuracy.append((get_accuracy(predictions, targets),
             get_accuracy(test_predictions, test_targets)))

  print("Exporting results...")
  # Finding confusion matrix.
  c_matrix = get_confusion_matrix(test_targets, test_predictions, 10)
  with open('2-half.csv', 'w') as f:
    np.savetxt(f, c_matrix, delimiter=',', fmt='%d')
    f.write('\n')
    np.savetxt(f, accuracy, delimiter=',', fmt='%f')
    f.write('\n')
    np.savetxt(f, hidden_weights, delimiter=',', fmt='%f')
    f.write('\n')
    np.savetxt(f, output_weights, delimiter=',', fmt='%f')
  print("All done!")
