import numpy as np
from scipy.special import expit
class Network:
  def __init__(self, amount_input_units, amount_hidden_units, 
               amount_output_units):
    self.amount_input_units = amount_input_units
    self.amount_hidden_units = amount_hidden_units
    self.amount_output_units = amount_output_units

    self.layers = []
    self.layers.append(Layer(amount_input_units))
    self.layers.append(Layer(amount_hidden_units))
    self.layers.append(Layer(amount_output_units))

    self.link_layers()


  def link_layers(self):
    for i in range(len(self.layers)):
      if i != len(self.layers)-1:
        self.layers[i].link_layer(self.layers[i+1])


  def weights_required(self):
    # How many weights this network requires to operate.
    # +1 to account for bias unit.
    return sum([layer.amount_required_weights for layer in self.layers])


  def assign_weights(self, weights):
    for layer in self.layers:
      weights = layer.retrieve_weights(weights)
    assert weights is None, "More weights were assigned than used!"


  def propagate(self, input_values):
    return self.layers[0].propagate_forward(input_values)
  


class Layer:
  def __init__(self, amount_units):
    self.amount_units = amount_units
    self.next_layer = None
    self.amount_required_weights = 0
    self.weights = []


  def propagate_forward(self, input_values):
    assert self.weights is not None, "Assign weights first!"

    if not self.next_layer:
      return input_values

    _inputs = np.insert(input_values, 0, 1)  # Extra input for bias.
    activation = expit([np.dot(self.weights[i], _inputs) \
                        for i in range(self.next_layer.amount_units)])
    return self.next_layer.propagate_forward(activation)


  def retrieve_weights(self, weights):
    # Set and remove weights from the given list.
    # Return unused weights.
    assert len(weights) >= self.amount_required_weights, \
           "Not enough weights were given!"
    if not self.next_layer:
      # Output layer don't need weights.
      return

    self.weights = weights[:self.amount_required_weights]
    self.weights = np.reshape(self.weights, 
                     (self.next_layer.amount_units, self.amount_units+1))
    return weights[self.amount_required_weights:]


  def link_layer(self, next_layer):
    # NOTE: Output layer do not call this.
    self.next_layer = next_layer
    self.amount_required_weights = (self.amount_units+1) * self.next_layer.amount_units
