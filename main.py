import numpy as np
from neuralnetwork import Network
from GA.GAWeightsManager import GAWeightsManager
from datetime import datetime
import random

np.set_printoptions(suppress=True)
output_units = 2
hidden_units = 5
weight_range = 2
population = 100
generations = 10
sample_amount = 450

def get_confusion_matrix(prediction, target):
  # Returns the matrix of actual target class to prediction.
  # matrix[target][prediction]

  assert(len(target) == len(prediction))
  matrix = np.zeros((output_units, output_units))
  for a, b in zip(target, prediction):
    matrix[int(a)][int(b)] += 1
  return matrix     


def process_input(inp):
  targets = inp[:, -1]
  data = np.true_divide(inp, np.max(inp))
  data = np.roll(data, 1, axis=1)
  data[:, 0] = 1
  return data, targets


def get_accuracy(ls1, ls2):
  # Returns percent match between ls1 and ls2.
  assert(len(ls1) == len(ls2))
  correct = 0
  for a, b in zip(ls1, ls2):
    if a == b:
      correct += 1
  return correct / len(ls1)


if __name__ == '__main__':
  train_inputs = np.genfromtxt('data/optdigits.train', delimiter=',')
  test_inputs = np.genfromtxt('data/optdigits.test', delimiter=',')
  
  input_size = len(train_inputs[0])
  network = Network(input_size, hidden_units, output_units)
  weights_required = network.weights_required()
  weights_ga = GAWeightsManager(population, weights_required, weight_range)
  
  np.random.shuffle(train_inputs)
  train_data, train_targets = process_input(train_inputs)
  test_data, test_targets = process_input(test_inputs)
  
  for generation in range(generations):
    start_time = datetime.now()
    print("Start of generation", generation)

    # Train genetic weights.
    sample_indices = random.sample(range(len(train_data)), sample_amount)
    training_accuracies = []
    for individual in range(population):
      predictions = []
      for i in sample_indices:
        network.assign_weights(weights_ga.getWeightsOfInd(individual))
        activation_values = network.propagate(train_data[i])
        predictions.append(np.argmax(activation_values))
      training_accuracies.append(get_accuracy(predictions, train_targets[sample_indices]))
    
    weights_ga.nextGeneration(np.array(training_accuracies), withElitism=True)
    
    # Get the accuracy of each set of weights.
    all_accuracies = []
    for i in range(population):
      predictions = [] 
      network.assign_weights(weights_ga.weights[i].tolist())
      for j in range(len(train_data)):
        activation_values = network.propagate(train_data[j])
        predictions.append(np.argmax(activation_values))
      all_accuracies.append(get_accuracy(predictions, train_targets))
    
    # Save set of weights that has best accuracy.
    previous_weights_ga = weights_ga.getBestGeneration(all_accuracies)
      
    # With the set of weights that had best accuracy, compute accuracy of test set.
    network.assign_weights(previous_weights_ga.tolist())
    predictions = []
    for i in range(len(test_data)):
        activation_values = network.propagate(test_data[i])
        predictions.append(np.argmax(activation_values))
        
    print("Train accuracy", round(np.max(all_accuracies)*100,2),"%")
    print("Test accuracy:", round(get_accuracy(predictions, test_targets)*100,2),"%")
    conf_matrix = get_confusion_matrix(predictions, test_targets)
    print(conf_matrix)
    print("Generation", generation,"took", (datetime.now() - start_time).seconds,"seconds to run")

